
//3
let num = prompt("Please input a number:");
let getCube = num **3;

//4
console.log(`The cube of ${num} is ${getCube}.`);

//5
let address = ["258","Washington Ave NW","California","9001"];
//6
let [houseNum,avenue,state,zip] = address;
console.log(`I live at ${houseNum} ${avenue}, ${state} ${zip}.`);

//7
let animal = {
	name: "Lolong",
	type: "saltwater crocodile",
	weight: 1075,
	height: "20ft 3in"
};

//8
let {name,type,weight,height} = animal;
console.log(`${name} was a ${type}. He weighed ${weight} kgs with a measurement of ${height}.`);

//9
let arrNum = [1,2,3,4,5];
//10
arrNum.forEach((number) => console.log(number));
//11
let reduceNumber = arrNum.reduce((x,y)=>x+y);
console.log(`reduce method: ${reduceNumber}`);
//12
class Dog{
	constructor(name,age,breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}
//13
let myDog = new Dog("Frankie",5,"Miniature Dachshund");
console.log(myDog);

